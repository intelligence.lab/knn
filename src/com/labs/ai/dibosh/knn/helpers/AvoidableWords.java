package com.labs.ai.dibosh.knn.helpers;

import java.util.ArrayList;
import java.util.StringTokenizer;

public class AvoidableWords {
	ArrayList<String> words = new ArrayList<String>();
	//arranged in order they will be concatenated during parsing in constructor
	//its important as the comma separation will be invalid nontheless
	String conjunctions = "and, but, or, nor, for, yet, so,although, because, since, unless,";
	String pronouns = "it,I,you,he,they,we,she,who,them,me,him,one,her,us,something,nothing,anything,himself,everything,someone,themselves,everyone,itself,anyone,myself,";
	String articles = "a,an,the,";
	String auxilliary_verbs = "will,shall,can,would,should,could,may,might,must,do,does,did,am,is,are,was,were,be,been,have,has,had,being,";
	String prepositions = "of,in,to,for,with,on,at,from,by,about,as,into,like,through,after,over,between,out,against,during,without,before,under,around,among";
	private static AvoidableWords avoidable_words_instance = null;
	/**
	 * Singleton instance
	 * @return
	 */
	public static AvoidableWords getInstance()
	{
		if(avoidable_words_instance == null)
		{
			avoidable_words_instance = new AvoidableWords();
		}
		return avoidable_words_instance;
	}
	private AvoidableWords()
	{
		String delim = ",";
		StringTokenizer tokens = new StringTokenizer(conjunctions+pronouns+auxilliary_verbs+articles+prepositions,delim);
		while(tokens.hasMoreTokens())
		{
			String word = tokens.nextToken();
			this.words.add(word);
		}
	}
	/**
	 * Checks if the word is avoidable.
	 * @param word
	 * @return
	 */
	public boolean isAvoidable(String word)
	{
		for(String wrd: words)
		{
			if(wrd.equalsIgnoreCase(word))
				return true;
		}
		return false;
	}

}
