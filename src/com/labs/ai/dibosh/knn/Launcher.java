package com.labs.ai.dibosh.knn;

import java.util.ArrayList;
import java.util.HashSet;
public class Launcher {

	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		
		
		
		
		String trainingFile = "test/training.data";
		String testFile = "test/test.data";
		NaiveByes program = new NaiveByes();
		try {
			program.load(trainingFile, testFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		int numOfTestDatas =10;
		
		program.start(1,20);

	}

}
