package com.labs.ai.dibosh.knn;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

import com.labs.ai.dibosh.knn.helpers.Util;
import com.labs.ai.dibosh.knn.objects.DocumentObject;

public class NaiveByes {
	HashMap<String,String> vocabulary = new HashMap<String, String>();
	HashMap<String, ArrayList<DocumentObject>> docsWithSameTopic = new HashMap<String, ArrayList<DocumentObject>>();
	ArrayList<String> topics = new ArrayList<String>();
	ArrayList<DocumentObject> trainingDocs = new ArrayList<DocumentObject>();
	ArrayList<DocumentObject> testDocs = new ArrayList<DocumentObject>();
	//key format - topic::word
	HashMap<String,Integer> totalWordsPerTopic = new HashMap<String, Integer>();
	
	HashMap<String,Float> classProbabilities = new HashMap<String, Float>();
	
	float correct = 0;
	
	private void extractTopics()
	{
		topics.clear();
		for(DocumentObject doc : trainingDocs)
		{
			if(!topics.contains(doc.getTopic()))
			{
				topics.add(doc.getTopic());
			}
		}
	}
	private void extractVocabulary()
	{
		vocabulary.clear();
		for(DocumentObject doc : trainingDocs)
		{
			for(String word : doc.getWords())
			{
				vocabulary.put(word, word);
			}
		}
	}
	/**
	 * Returns the total word count under certain topic
	 * @param topic
	 * @return
	 */
	private void extractTotalWordCountForEachTopic()
	{
		int count = 0;
		for(String topic : topics)
		{
			ArrayList<DocumentObject> docs = docsWithSameTopic.get(topic);
			for(DocumentObject doc: docs)
			{
				for(String word : doc.getWords())
				{
					count += doc.getCountOfAWord(word);
				}
			}
			totalWordsPerTopic.put(topic, count);
			count = 0;
		}
	}
	private int getFrequencyOfTheWordInTopic(String word,String topic)
	{
		int count = 0;
		for(DocumentObject doc: trainingDocs)
		{
			if(doc.getWords().contains(word) && doc.getTopic().equalsIgnoreCase(topic))
			{
				count += doc.getCountOfAWord(word);
			}
		}
		
		return count;
	}
	private void extractDocsWithSameTopic()
	{
		docsWithSameTopic.clear();
		for(String topic : topics)
		{
			ArrayList<DocumentObject> sameTopicDocs = new ArrayList<DocumentObject>();
			for(DocumentObject doc : trainingDocs)
			{
				if(doc.getTopic().equalsIgnoreCase(topic))
				{
					sameTopicDocs.add(doc);
				}
			}
			docsWithSameTopic.put(topic, sameTopicDocs);
		}
	}
	/**
	 * Start the main classifier
	 */
	public void start(int smoothingFactor,int noOfTestData)
	{
		System.out.println("Extracting important features...");
		extractVocabulary();
		extractTopics();
		extractDocsWithSameTopic();
		extractTotalWordCountForEachTopic();
		System.out.println("Processing "+noOfTestData+" test documents...");
		int count = 0;
		for(DocumentObject doc : testDocs)
		{
			if(count == noOfTestData) break;
			String detectedTopic = trainMultinominalNB(doc,smoothingFactor);
			Util.log(detectedTopic);
			if(detectedTopic.equalsIgnoreCase(doc.getTopic()))
			{
				correct++;
			}
			count ++;
		}
		double accuracy = correct/noOfTestData*1.0;
		
		Util.log("Detected "+correct+" documents properly");
		System.out.println("Accuracy:"+accuracy*100);
	}
	private String trainMultinominalNB(DocumentObject test,int smoother)
	{
		double max = -355555.0f;
		String detectedTopic = "";
		int N = trainingDocs.size();
		for(String topic : topics)
		{
			//docs with same topic
			int NC = docsWithSameTopic.get(topic).size();
			double probabilityOfTopic = Math.log(NC*1.0/N*1.0);
//			classProbabilities.put(topic, probabilityOfTopic);
			for(String word : test.getWords())
			{
				int countOfAllWords = totalWordsPerTopic.get(topic);
				double condProb = Math.log(getConditionalProbability(word, topic, countOfAllWords,smoother));
				Util.log(""+condProb);
				condProb = condProb * test.getCountOfAWord(word);
				probabilityOfTopic += condProb;
			}
			if(max < probabilityOfTopic) 
			{
				max = probabilityOfTopic;
				detectedTopic = topic;
			}
			
		}
		
		return detectedTopic;
	}
	
	private double getConditionalProbability(String word,String topic,int totalWordCount,int smoother)
	{
		double result = 0;
		result = (getFrequencyOfTheWordInTopic(word,topic)+smoother)*1.0/(totalWordCount+smoother*trainingDocs.size())*1.0;
		return result;
	}
	/**
	 * Start training from file
	 * @param trainingFile
	 * @param testFile TODO
	 * @throws IOException
	 */
	public void load(String trainingFile, String testFile) throws IOException
	{
		trainingDocs.clear();
		testDocs.clear();
		//will create a border in between each doc string
		String delim = "@@@@@@@@@@@@@@@@@@@@@@@@";
		String regex = "[\r\n|\r|\n]{3,}";
		
		String file_training = read(trainingFile);
		String formattedData = file_training.replaceAll(regex, Util.NL+delim+Util.NL);
		StringTokenizer tokens = new StringTokenizer(formattedData,delim);
		int total = tokens.countTokens();
		while(tokens.hasMoreTokens())
		{
			String docInstance = tokens.nextToken();
			String[] items = docInstance.split("[\r\n|\r|\n]{2,3}");
			if(items.length == 4)
			{
				//we only need item at 0 at 3rd index
				DocumentObject obj = new DocumentObject(items[0], items[3]);
				trainingDocs.add(obj);
			}
		}
		
		Util.log("Finished reading and parsing "+trainingDocs.size()+" documents."+(total-trainingDocs.size())+" documents are ignored for not" +
				" complying with specified format.");
		//test file
		String file_test = read(testFile);
		String formattedData2 = file_test.replaceAll(regex, Util.NL+delim+Util.NL);
		StringTokenizer tokens2 = new StringTokenizer(formattedData2,delim);
		int total2 = tokens2.countTokens();
		while(tokens2.hasMoreTokens())
		{
			String docInstance = tokens2.nextToken();
			String[] items = docInstance.split("[\r\n|\r|\n]{2,3}");
			if(items.length == 4)
			{
				//we only need item at 0 at 3rd index
				DocumentObject obj = new DocumentObject(items[0], items[3]);
				testDocs.add(obj);
			}
		}
		
		Util.log("Finished reading and parsing "+testDocs.size()+" documents."+(total2-testDocs.size())+" documents are ignored for not" +
				" complying with specified format.");
		
	}
	
	String read(String fileName) throws IOException {
	    Util.log("\nReading from file :"+fileName+"\n");
	    StringBuilder doc = new StringBuilder();
	    try {
	        //use buffering, reading one line at a time
	        //FileReader always assumes default encoding is OK!
	        BufferedReader input =  new BufferedReader(new FileReader(fileName));
	        
	        try {
	          String line = null; //not declared within while loop
	          while (( line = input.readLine()) != null){
	        	  doc.append(line);
	        	  doc.append(Util.NL);
	          }
	        }
	        finally {
	          input.close();
	        }
	      }
	      catch (IOException ex){
	        ex.printStackTrace();
	      }
	    return doc.toString();
	}

}
