package com.labs.ai.dibosh.knn.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;
import java.util.StringTokenizer;

import com.labs.ai.dibosh.knn.helpers.AvoidableWords;

public class DocumentObject {
	String topic;
	String content;
	HashMap<String,Integer> WordMap = new HashMap<String, Integer>();
	
	//holds hamming distance with any(only one)instance of this type of object
	public int hammingDistance = 0;
	//holds hamming distance with any(only one)instance of this type of object
	public int euclidianDistance = 0;
	
	public DocumentObject(String tpc,String cntnt)
	{
		this.topic = tpc;
		this.content = cntnt;
		generateWordCounts();
	}
	public String getTopic()
	{
		return topic;
	}
	public boolean isSameTopic(DocumentObject obj)
	{
		return this.getTopic().equalsIgnoreCase(obj.getTopic());
	}
	/**
	 * Returns the total number of words in the document
	 * @return
	 */
	public int getNumberOfWords()
	{
		return WordMap.size();
	}
	/**
	 * How many times the word is in the doc.Frequency of the word.
	 * @param word
	 * @return
	 */
	public int getCountOfAWord(String word)
	{
		return this.WordMap.get(word);
	}
	/**
	 * Returns the common words in between these 2 documents
	 * @param obj
	 * @return
	 */
	public ArrayList<String> getCommonWordsList(DocumentObject obj)
	{
		ArrayList<String> result = new ArrayList<String>();
		for(String word:this.WordMap.keySet())
		{
			if(obj.isCommon(word))
			{
				result.add(word);
			}
		}
		return result;
	}
	/**
	 * Returns euclidian distance between these 2 documents
	 * @param obj
	 * @return
	 */
	public int getEuclidianDistance(DocumentObject obj)
	{
		double distance=0;
		ArrayList<String> commonWords = this.getCommonWordsList(obj);
		//do the calculation for common words at first
		for(String word:commonWords)
		{
			long diff = Math.abs(this.getCountOfAWord(word) - obj.getCountOfAWord(word));
			distance += diff*diff;
		}
		//do for other available words from this doc
		for(String word:this.getWords())
		{
			if(commonWords.contains(word))continue;//done before,so skip
			distance += this.getCountOfAWord(word)*this.getCountOfAWord(word);
		}
		//do for other available words from obj doc
		for(String word:obj.getWords())
		{
			if(commonWords.contains(word))continue;//done before,so skip
			distance += obj.getCountOfAWord(word)*obj.getCountOfAWord(word);
		}
		
		return (int)Math.sqrt(distance);
	}
	/**
	 * Used to get the set of words in this doc.
	 * @return
	 */
	public Set<String> getWords()
	{
		return this.WordMap.keySet();
	}
	/**
	 * Hamming Distance between this and obj document.
	 * @param obj
	 * @return
	 */
	public int getHammingDistance(DocumentObject obj)
	{
		return this.getNumberOfWords() + obj.getNumberOfWords() - 2*getNumberOfCommonWords(obj);
	}
	/**
	 * Checks if this word is available in this documents word set.
	 * @param word
	 * @return
	 */
	public boolean isCommon(String word)
	{
		return this.WordMap.containsKey(word);
	}
	/**
	 * Return the number of common words between given object and this.
	 * @param obj
	 * @return
	 */
	public int getNumberOfCommonWords(DocumentObject obj)
	{
		int count = 0;
		for(String word:this.WordMap.keySet())
		{
			//word is also present in obj doc
			if(obj.isCommon(word))
			{
				count++;
			}
		}
		
		return count;
	}
	private void generateWordCounts()
	{
		String delim = " ";
		StringTokenizer tokens = new StringTokenizer(content,delim);
		while(tokens.hasMoreTokens())
		{
			String word = tokens.nextToken();
			//if this is one of the avoidable words,skip it.
			if(AvoidableWords.getInstance().isAvoidable(word)) continue;
			//the word is new
			if(!this.WordMap.containsKey(word))
			{
				this.WordMap.put(word, 1);
			}
			else
			{
				//the word is previously available,increase count
				int count = this.WordMap.get(word) + 1;
				this.WordMap.put(word, count);
			}
		}
	}

}
