package com.labs.ai.dibosh.knn;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;

import com.labs.ai.dibosh.knn.helpers.Util;
import com.labs.ai.dibosh.knn.objects.DocumentObject;

public class MainProgram {
	
	static int hCorrectGuess = 0;
	static int hIncorrectGuess = 0;
	static int eCorrectGuess = 0;
	static int eIncorrectGuess = 0;
	ArrayList<DocumentObject> trainingDocs = new ArrayList<DocumentObject>();
	ArrayList<DocumentObject> testDocs = new ArrayList<DocumentObject>();
	//documents with hamming distance calculated with respect to some document
	ArrayList<DocumentObject> hDocs = new ArrayList<DocumentObject>();
	//documents with euclidian distance calculated with respect to some document
	ArrayList<DocumentObject> eDocs = new ArrayList<DocumentObject>();
	//comparators-needed to sort
	Comparator<DocumentObject> hammingComparator = new Comparator<DocumentObject>() {
		
		@Override
		public int compare(DocumentObject o1, DocumentObject o2) {
			return o1.hammingDistance - o2.hammingDistance;
		}
	};
	Comparator<DocumentObject> euclidianComparator = new Comparator<DocumentObject>() {
		
		@Override
		public int compare(DocumentObject o1, DocumentObject o2) {
			return o1.euclidianDistance - o2.euclidianDistance;
		}
	};
	
	/**
	 * Start training from file
	 * @param trainingFile
	 * @param testFile TODO
	 * @throws IOException
	 */
	public void load(String trainingFile, String testFile) throws IOException
	{
		trainingDocs.clear();
		testDocs.clear();
		//will create a border in between each doc string
		String delim = "@@@@@@@@@@@@@@@@@@@@@@@@";
		String regex = "[\r\n|\r|\n]{3,}";
		
		String file_training = read(trainingFile);
		String formattedData = file_training.replaceAll(regex, Util.NL+delim+Util.NL);
		StringTokenizer tokens = new StringTokenizer(formattedData,delim);
		int total = tokens.countTokens();
		while(tokens.hasMoreTokens())
		{
			String docInstance = tokens.nextToken();
			String[] items = docInstance.split("[\r\n|\r|\n]{2,3}");
			if(items.length == 4)
			{
				//we only need item at 0 at 3rd index
				DocumentObject obj = new DocumentObject(items[0], items[3]);
				trainingDocs.add(obj);
			}
		}
		
		Util.log("Finished reading and parsing "+trainingDocs.size()+" documents."+(total-trainingDocs.size())+" documents are ignored for not" +
				" complying with specified format.");
		//test file
		String file_test = read(testFile);
		String formattedData2 = file_test.replaceAll(regex, Util.NL+delim+Util.NL);
		StringTokenizer tokens2 = new StringTokenizer(formattedData2,delim);
		int total2 = tokens2.countTokens();
		while(tokens2.hasMoreTokens())
		{
			String docInstance = tokens2.nextToken();
			String[] items = docInstance.split("[\r\n|\r|\n]{2,3}");
			if(items.length == 4)
			{
				//we only need item at 0 at 3rd index
				DocumentObject obj = new DocumentObject(items[0], items[3]);
				testDocs.add(obj);
			}
		}
		
		Util.log("Finished reading and parsing "+testDocs.size()+" documents."+(total2-testDocs.size())+" documents are ignored for not" +
				" complying with specified format.");
	}
	
	public void startMakingDecision(int k, int numOfTests)
	{
		Util.log("Started main process for k="+k+"...");
		int count = 0;
		hCorrectGuess = 0;
		eCorrectGuess =0;
		for(DocumentObject doc:this.testDocs)
		{
			count++;
			if(count == numOfTests) break;
			for(DocumentObject compared : this.trainingDocs)
			{
				compared.hammingDistance = compared.getHammingDistance(doc);
				compared.euclidianDistance = compared.getEuclidianDistance(doc);
				this.hDocs.add(compared);
				this.eDocs.add(compared);
			}
			Collections.sort(this.hDocs, hammingComparator);
			Collections.sort(this.eDocs,euclidianComparator);
			
			updateAccuracy(k, doc);
		}
		
		double haccuracy = (hCorrectGuess*1.0/numOfTests*1.0)*100;
		double eaccuracy = (eCorrectGuess*1.0/numOfTests*1.0)*100;
		Util.log("Hamming Distance Accuracy for k="+k+":"+hCorrectGuess+" documents were successfully identified! Accuracy:"+haccuracy);
		Util.log("Euclidian Distance Accuracy for k="+k+":"+eCorrectGuess+" documents were successfully identified! Accuracy:"+eaccuracy);
	}
	private void updateAccuracy(int k, DocumentObject doc) {
		int index = 0;
		if(k > 1)index = getIndexOfDocWithMaxCount(k);
			//according to hamming
			if(doc.isSameTopic(this.hDocs.get(index)))
			{
				hCorrectGuess++;
			}
			else
			{
				hIncorrectGuess++;
			}
			
			if(doc.isSameTopic(this.eDocs.get(index)))
			{
				eCorrectGuess++;
			}
			else
			{
				eIncorrectGuess++;
			}
			
	}
	
	private int getIndexOfDocWithMaxCount(int k) {
		int[] count ={0,0,0,0,0};//defining an array with max k = 5;
		for(int i=0;i<k;i++)
		{
			String topic = this.hDocs.get(i).getTopic();
			for(int j=0;j<k;j++)
			{
				if(this.hDocs.get(j).getTopic().equals(topic))
				{
					count[i]++;
				}
			}
		}
		//find out the index of the maximum countered doc
		int max = 0;
		int index =0;
		for(int i =0;i<k ;i++)
		{
			if(count[i]>max)
			{
				max = count[i];
				index = i;
			}
		}
		return index;
	}
	
	
	String read(String fileName) throws IOException {
	    Util.log("\nReading from file :"+fileName+"\n");
	    StringBuilder doc = new StringBuilder();
	    try {
	        //use buffering, reading one line at a time
	        //FileReader always assumes default encoding is OK!
	        BufferedReader input =  new BufferedReader(new FileReader(fileName));
	        
	        try {
	          String line = null; //not declared within while loop
	          while (( line = input.readLine()) != null){
	        	  doc.append(line);
	        	  doc.append(Util.NL);
	          }
	        }
	        finally {
	          input.close();
	        }
	      }
	      catch (IOException ex){
	        ex.printStackTrace();
	      }
	    return doc.toString();
	}
	  

}
